var persona ={
  esMayorDeEdad: true,
  edad: 23,
  nombre: {
    primero: 'Hemanuel',
    segundo: 'noTengo',
    apellido: 'DeJesus Perez'
  },
  comidaPreferidas: [//array/Lista
    'Carnita Asada',
    'Tacos de Bistek',
    'Cerebros Humanos',
    'Piratas'
  ],
  fechaActual: Date.now()
};
console.log(persona);


/* boleano: sexo
numero: edad
objeto: nombre ,primero,segundo, ap,
array: comidas ["hxcn","hdnxgb0"]
respiroMasReciente: Date.now() */



var Monstruo = function(name){
  this.nombre = nombre || 'monstruo';
  this.tamaño = 'XL';
  this.gruñir =function(){
    alert('GRUÑIDO MATON')
  }
};
var Monstruito = function(){
  this.tamaño = 'XS';
  this.gorrita = true;
  this.biberon = true;
}

Monstruito.prototype = new Monstruo();
var pepe = new Monstruito();
